if [[ -z $1 ]]; then
	FILES=*
else
	FILES=$1
fi
clear
for f in ../parser/$FILES; do 
	echo 
	echo =========================
	echo Testando: $f
	echo =========================
	java -jar dist/Compiler.jar -target parse $f
	echo 
done
