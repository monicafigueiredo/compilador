if [[ -z $1 ]]; then
	FILES=*
else
	FILES=$1
fi
clear
for f in ../semantics/$FILES; do 
	echo 
	echo =========================
	echo Testando: $f
	echo =========================
	java -jar dist/Compiler.jar -target inter $f
	echo 
done
