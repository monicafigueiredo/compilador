lexer grammar DecafLexer;

@header {
package decaf;
}

options {
  language=Java;
}

tokens {
  TK_class
}

LCURLY : '{';
RCURLY : '}';
LPARENT : '(';
RPARENT : ')';
LCOLC : '[';
RCOLC : ']';
COMMA : ',';
SEMICOLON : ';';

TK_class : 'class Program';

EQUAL_SIGN : '=';
MINUS_SIGN : '-';

ASSIGN_OPERATOR : '+=' | '-=';

A_OPERATOR : '+' | '*' | '/' | '%';
COMPARATOR : '<' | '<=' | '>' | '>=';
L_OPERATOR : '&&' | '||' | '!=' | '==';
N_OPERATOR : '!';


WS_ : (' ' | '\n' | '\t') -> skip;

SL_COMMENT : '//' (~'\n')* '\n' -> skip;

CHAR_T : '\'' (CHARLIT) '\'';
STRING_T : '"' (ESC|STRINGLIT)*  '"';

BOOLEAN_T : 'false' | 'true';

INT_T : HEX | INTLIT;

HEX_CEPTION : '0x';

IF_E : 'if';
BOOLEAN_TP : 'boolean';
CALLOUT_E : 'callout';
CLASS_E : 'class';
ELSE_E : 'else';
INT_TP : 'int';
RETURN_E : 'return';
VOID_T : 'void';
FOR_E : 'for';
BREAK_E : 'break';
CONTINUE_E : 'continue';

ID : [a-zA-Z_]+[a-zA-Z_0-9]*;

fragment DOT : '.';
fragment TEXT_MARKS : '?' | '!' | ':' | COMMA | DOT | SEMICOLON | EQUAL_SIGN | A_OPERATOR;
fragment ESC :  '\\' ('n'|'"'|'t'|'\\'|'\'');
fragment CHARLIT : [a-zA-Z0-9] | ESC;
fragment HEX : '0x'HEXLIT+;
fragment HEXLIT : [0-9a-fA-F];
fragment INTLIT : [0-9]+;
fragment STRINGLIT : [0-9a-zA-Z ] | TEXT_MARKS;
