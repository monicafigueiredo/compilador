parser grammar DecafParser;

@header {
package decaf;
}

options {
  language=Java;
  tokenVocab=DecafLexer;
}

program: 	TK_class LCURLY var_decl* method_decl* RCURLY EOF;

block: 		LCURLY (var_decl | statement)* RCURLY;

var_decl: 	var_sequence SEMICOLON;

array_value:	ID | INT_T;

location:	ID 
		| ID LCOLC array_value RCOLC;

asgn_operator:	ASSIGN_OPERATOR | EQUAL_SIGN;

statement: 	(location asgn_operator expr
		| method_call
		| RETURN_E expr?
		| BREAK_E) SEMICOLON
		| IF_E LPARENT expr RPARENT block (ELSE_E block)?
		| FOR_E ID EQUAL_SIGN expr COMMA expr block;

var_sequence:	type var_name_decl;

var_name_decl: 	(ID | ID LCOLC array_value RCOLC) (COMMA var_name_decl)*;

type: 		INT_TP
		| BOOLEAN_TP;

param:		type ID;

method_decl: 	(type | VOID_T) ID LPARENT (param (COMMA param)*)? RPARENT block;

callout_arg:	STRING_T
		| expr;

method_call:	ID LPARENT (expr | expr (COMMA expr)+)? RPARENT
		| CALLOUT_E LPARENT STRING_T (COMMA callout_arg)* RPARENT;

literal:	INT_T
		| BOOLEAN_T
		| CHAR_T;

bin_op:		A_OPERATOR
		| COMPARATOR
		| L_OPERATOR
		| MINUS_SIGN;


expr:		location
		| method_call
		| literal
		| expr bin_op expr
		| MINUS_SIGN expr
		| N_OPERATOR expr
		| LPARENT expr RPARENT;


























