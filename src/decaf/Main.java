package decaf;

import java.io.*;
//import antlr.Token;
import java.util.Arrays;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java6035.tools.CLI.*;
import decaf.DecafSymbolsAndScopes;

class Main {
    public static void main(String[] args) {
        try {
        	CLI.parse (args, new String[0]);

        	InputStream inputStream = args.length == 0 ?
                    System.in : new java.io.FileInputStream(CLI.infile);

        	if (CLI.target == CLI.SCAN)
        	{
        		DecafLexer lexer = new DecafLexer(new ANTLRInputStream(inputStream));
        		Token token;
        		boolean done = false;
        		while (!done)
        		{
        			try
        			{
		        		for (token=lexer.nextToken(); token.getType()!=Token.EOF; token=lexer.nextToken())
		        		{
		        			String type = "";
		        			String text = token.getText();

		        			switch (token.getType()){
		        			case DecafLexer.ID:
		        				type = "IDENTIFIER";
		        				break;
						case DecafLexer.CHAR_T:
							type = "CHARLITERAL";
							break;
						case DecafLexer.BOOLEAN_T:
							type = "BOOLEANLITERAL";
							break;
						case DecafLexer.STRING_T:
							type = "STRINGLITERAL";
							break;
						case DecafLexer.INT_T:
							type = "INTLITERAL";
							break;
						case DecafLexer.SL_COMMENT:
							type = "SL_COMMENT";
							break;
						case DecafLexer.MINUS_SIGN:
							type = "MINUS_SIGN";
							break;
						case DecafLexer.EQUAL_SIGN:
							type = "EQUAL_SIGN";
							break;
						case DecafLexer.N_OPERATOR:
							type = "N_OPERATOR";
							break;
						case DecafLexer.A_OPERATOR:
						case DecafLexer.L_OPERATOR:
						case DecafLexer.COMPARATOR:
							type = "OPERATOR";
							break;
						case DecafLexer.LCURLY:
						case DecafLexer.RCURLY:
							type = "CURLY";
							break;
						case DecafLexer.LPARENT:
						case DecafLexer.RPARENT:
							type = "PARENT";
							break;
						case DecafLexer.LCOLC:
						case DecafLexer.RCOLC:
							type = "COLC";
							break;
						case DecafLexer.ASSIGN_OPERATOR:
							type = "ASSIGN_OPERATOR";
							break;
						case DecafLexer.COMMA:
							type = "COMMA";
							break;
						case DecafLexer.SEMICOLON:
							type = "SEMICOLON";
							break;
						case DecafLexer.IF_E:
							type = "IF_E";
							break;
						case DecafLexer.BOOLEAN_TP:
							type = "BOOLEAN_TP";
							break;
						case DecafLexer.CALLOUT_E:
							type = "CALLOUT_E";
							break;
						case DecafLexer.CLASS_E:
							type = "CLASS_E";
							break;
						case DecafLexer.ELSE_E:
							type = "ELSE_E";
							break;
						case DecafLexer.INT_TP:
							type = "INT_TP";
							break;
						case DecafLexer.RETURN_E:
							type = "RETURN_E";
							break;
						case DecafLexer.VOID_T:
							type = "VOID_T";
							break;
						case DecafLexer.FOR_E:
							type = "FOR_E";
							break;
						case DecafLexer.BREAK_E:
							type = "BREAK_E";
							break;
						case DecafLexer.CONTINUE_E:
							type = "CONTINUE_E";
							break;
						case DecafLexer.HEX_CEPTION:
							throw new HexCeption("unexpected char: 0xA");
		        			}
		        			System.out.println (token.getLine() + " " + type + " " + text);
		        		}
		        		done = true;
        			}
				catch(HexCeption e){
					System.out.printf("%s", e);
				}
				catch(Exception e) {
        	        	// print the error:
        	            System.out.println(CLI.infile+" "+e);
        	            lexer.skip();
        	        }
        		}
        	}
        	else if (CLI.target == CLI.PARSE || CLI.target == CLI.DEFAULT)
        	{
        	    	//Primeiro faz o parsing da cadeia
		        DecafLexer lexer = new DecafLexer(new ANTLRInputStream(inputStream));
		        CommonTokenStream tokens = new CommonTokenStream(lexer);
		        DecafParser parser = new DecafParser(tokens);

		        // Adiciona as regras semÃ¢nticas
		        ParseTree tree = parser.program();

		        if (CLI.debug) {
		            // Se estiver no modo debug imprime a Ã¡rvore de parsing
		            // Create Tree View
		            // Source: https://stackoverflow.com/questions/23809005/how-to-display-antlr-tree-gui
			    // How to load tree: java -jar dist/Compiler.jar -target parse -debug ../parser/illegal-02

		            //show AST in console
		            System.out.println(tree.toStringTree(parser));

		            //show AST in GUI
		            JFrame frame = new JFrame("Antlr AST");
		            JPanel panel = new JPanel();
		            TreeViewer viewr = new TreeViewer(Arrays.asList(
		                    parser.getRuleNames()),tree);
		            viewr.setScale(1);//scale a little
		            panel.add(viewr);
		            frame.add(panel);
		            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		            frame.setSize(200,200);
		            frame.setVisible(true);
		        }

            } else if (CLI.target == CLI.INTER) {
				// Primeiro faz o parsing da cadeia
				DecafLexer lexer = new DecafLexer(new ANTLRInputStream(inputStream));
				CommonTokenStream tokens = new CommonTokenStream(lexer);
				DecafParser parser = new DecafParser(tokens);

				// Adiciona as regras semânticas
				ParseTree tree = parser.program();

				// Realiza o parsing do programa
				DecafSymbolsAndScopes def = new DecafSymbolsAndScopes();
				ParseTreeWalker walker = new ParseTreeWalker();
				walker.walk(def, tree);

				if (CLI.debug) {
					// Se estiver no modo debug imprime a árvore de parsing
					// Create Tree View
					// Source: https://stackoverflow.com/questions/23809005/how-to-display-antlr-tree-gui


					//show AST in console
					System.out.println(tree.toStringTree(parser));

					//show AST in GUI
					JFrame frame = new JFrame("Antlr AST");
					JPanel panel = new JPanel();
					TreeViewer viewr = new TreeViewer(Arrays.asList(
							parser.getRuleNames()),tree);
					viewr.setScale(1.5);//scale a little
					panel.add(viewr);
					frame.add(panel);
					frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					frame.setSize(600,400);
					frame.setVisible(true);
				}

			}

        } catch(Exception e) {
        	// print the error:
            System.out.println(CLI.infile+" "+e);
        }
    }
}

class HexCeption extends Exception {

	public HexCeption(String message){
		super(message);	
	}

}
